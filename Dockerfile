FROM openjdk
ARG JAR_NAME
ENV JAR_NAME ${JAR_NAME}
COPY ./target/${JAR_NAME} /
CMD java -jar ${JAR_NAME}